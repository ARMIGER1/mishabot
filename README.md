#MishaBot

An IRC bot for the /r/katawashoujo community.

## Running

Before running, add a ```.env``` file with the following
information:

```
IRC_SERVER=localhost
CHANNEL=example_channel
PLUGINS=Greet, Help
```

This will allow you to test MishaBot on your own computer.  To run on a different server, just change the environment variable accordingly.

To actually run MishaBot, simply run ```foreman start``` in your terminal and Foreman will take care of everything else.

##Features

* Greets users when they enter the channel
* Rate-limits greetings for users with bad connections.
* Misses users when they leave the channel
* Announces when a user has feels to share
