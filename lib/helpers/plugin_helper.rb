module Helpers
  # Namespace for classes and modules that help with plugins
  #
  # @author ARMIGER1
  #
  # @since 4.0
  module PluginHelper
    def env_to_class_array(env_variable)
      values = []

      values = env_variable.split(', ').map! do |string|
        Kernel.const_get(string.to_sym)
      end

      values
    end
  end
end
