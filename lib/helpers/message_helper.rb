# Namespace for classes and modules that handle helping other parts of the
# codebase.
#
# @since 1.1
module Helpers
  # Namespace for classes and modules that handle helping messages.
  #
  # @since 1.1
  module Message
    # @private
    BEGIN_HELP = '***** MishaBot Help *****'

    # @private
    END_HELP = '***** End Help *****'

    # Builds the bot's main command help message.
    #
    # @author ARMIGER1
    #
    # @param [Hash{Symbol => String}] commands the commands that can currently
    #   be used by the bot, where the keys are the command names and the values
    #   are their brief descriptions.
    #
    # @example Command help message with one (1) method called 'foo'
    #   command_help_message(foo: 'This is a description of foo.')
    #
    # @example Command help message with two (2) methods called 'foo' and 'bar'
    #   command_help_message(
    #     foo: 'This is a description of foo.',
    #     bar: 'This is a description for bar.')
    #
    # @return [String] the bot's main command help message
    def command_help_message(commands = {})
      command_string = ''

      if commands.empty?
        command_string = ''
      else
        commands.each do |command, description|
          command_string << "  #{Format(:bold, command.to_s.upcase)}    #{description}\n"
        end
      end

      helpmsg = <<-EOT.gsub(/^ {6}/) { '' }.chomp
      #{BEGIN_HELP}
      #{LAUGH}  Looks like you need some help!  I respond to the following
      commands:

      #{command_string.empty? ? "I don't respond to anything yet..." : command_string.chomp}

      To get help on any command, just type: /msg MishaBot help <command>
      #{END_HELP}
      EOT

      helpmsg
    end

    # Builds a plugin's individual help message.
    #
    # @author ARMIGER1
    #
    # @param [String] command the command name
    # @param [String] description short description of the command
    # @param [String] syntax syntax to invoke the command
    # @param [String] examples examples of how to invoke the command
    # @param [String] notes extra notes that should be displayed to the user.
    #
    # @example Help message for a plugin called 'foo'
    #   help_message(
    #     'foo',
    #     'description of foo',
    #     'FOO <arg1>',
    #     '/msg MishaBot FOO arg1')
    #
    # @example Help message with notes for a plugin called 'bar'
    #   help_message(
    #     'bar',
    #     'description of bar',
    #     '/msg MishaBot BAR arg1',
    #     'Note regarding bar.')
    def help_message(command, description, syntax, examples, notes = '')
      helpmsg = <<-EOT.gsub(/^ {6}/) { '' }.chomp
      #{BEGIN_HELP}
      Help for #{Format(:bold, command.upcase)}

      #{Format(:bold, command.upcase)}    #{description}

      Syntax: #{syntax}

      #{LAUGH}  Here's some examples:

      #{examples}#{"\n\n" << "NOTE: " << notes unless notes.nil?}

      #{END_HELP}
      EOT

      helpmsg
    end

    # Executes the code inside the given block after the specified number of
    # seconds.
    #
    # @author ARMIGER1
    #
    # @param [Integer, Float] time time in seconds to wait before all code
    #   inside the block is executed.
    # @param [Integer] shots number of times the code inside the block should be
    #   executed.
    # @yield [Object] code to be executed.
    #
    # @note Anything put inside the block will be executed AFTER the given
    #   amount of time has passed.
    #
    # @since 4.1.1
    def rate_limit(time, shots, &block)
      Timer(time, shots: shots) do
        yield
      end
    end
  end
end
