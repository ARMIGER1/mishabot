require 'pry'
require 'require_all'

require 'cinch'

require_all 'lib/config'
require_all 'lib/helpers'
require_all 'lib/plugins'

$mishabot = Cinch::Bot.new do
  configure do |c|
    # The server to connect to.
    c.server = ENV['IRC_SERVER']

    # The bot's IRC nick.
    c.nick = 'MishaBot'

    # The channel for the bot to join
    c.channels = ["#{CHANNEL}"]

    # The default plugin prefix (!) which interferes with other bots.
    c.plugins.prefix = ''

    # Plugins to use
    c.plugins.plugins = env_to_class_array(ENV['PLUGINS'])
  end
end

$mishabot.start
