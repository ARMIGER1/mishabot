# A class for the STREAM command.
#
# @author ARMIGER1
# @since 3.1
class Stream
  include Cinch::Plugin

  ## Regexes

  # @private
  STREAM_REGEX = 'stream (?<hours>[\d\.]+)?(?<game>[\w\ \:]+) (?<url>[a-z]{3,}:[\/]{2}[\w\.\/\-\=?#]+)'

  # @private
  STREAM_EXAMPLES = <<-EOT.gsub(/^ {4}/) { '' }.chomp
  /msg MishaBot stream Katawa Shoujo http://hitbox.tv/hisao_nakai
  /msg MishaBot stream 0.25 Hatoful Boyfriend http://twitch.tv/miki_miura
  EOT

  match(/#{STREAM_REGEX}/i, react_on: :private, method: :stream)
  match(/^streams$/i, react_on: :private, method: :streams)
  match(/^endstream$/i, react_on: :private, method: :stream_end)

  def initialize(*args)
    super

    @streams = {}

    @rate_limited = {stream: [], streams: []}

    $mishabot.config.plugins.options[Stream] = {
      plugin: 'stream',
      commands: {
        stream: {
          help_message: "Lets all users know you're streaming a game.",
          syntax: 'STREAM [time_in_hours] <game_title> <url>',
          examples: STREAM_EXAMPLES,
          notes: "If no time is specified, your stream will stay active for one (1) hour."
        },
        streams: {
          help_message: 'Shows everyone the list of currently active streams.',
          syntax: 'STREAMS',
          examples: '/msg MishaBot streams'
        },
        endstream: {
          help_message: 'Removes your currently active stream from the stream list.',
          syntax: 'STREAM END',
          examples: '/msg MishaBot endstream'
        }
      }
    }
  end

  # Adds a stream to the stream list.
  #
  # @author ARMIGER1
  #
  # @param [Cinch::Message] m the message sent by a user.
  # @param [Fixnum, String] hours the number of hours to stream for.
  # @param [String] game the game being streamed.
  # @param [String] url the URL where the game is being streamed.
  #
  # @since 4.0
  def stream(m, hours, game, url)
    @channel = Channel("#{CHANNEL}")

    hours = hours.nil? ? 1 : hours.to_f

    unless @rate_limited[:stream].include?(m.user)
      @rate_limited[:stream] << m.user

      @streams[m.user] = [game, url]

      @channel.send("#{m.user} is streaming #{game} at #{url} !")

      rate_limit 30, 1 do
        @rate_limited[:stream].delete(m.user)
        m.reply "You can use the STREAM command again.  Yay-yay!  ^_^"
      end

      rate_limit (hours * (60**2)), 1 do
        stream_end(m)
      end
    end
  end

  # Lists all currently active streams posted by users from the STREAM command.
  #
  # @author ARMIGER1
  #
  # @param [Cinch::Message] m the message sent by a user.
  def streams(m)
    @channel = Channel("#{CHANNEL}")

    unless @rate_limited[:streams].include?(m.user)
      @rate_limited[:streams] << m.user

      m.reply("***** It's the stream list! #{LAUGH} *****")

      if @streams.empty?
        m.reply("There's no streams right now...")
      else
        @streams.each do |user, game_data|
          m.reply("#{user} is streaming #{game_data[0]} at #{game_data[1]}")
        end
      end

      m.reply("***** END STREAM LIST *****")

      rate_limit (60 * 15), 1 do
        @rate_limited[:streams].delete(m.user)
        m.reply "You can use the STREAMS command again.  Yay-yay~!  ^_^"
      end
    end
  end

  # Removes the specified user's currently active stream from the stream list.
  #
  # @author ARMIGER1
  #
  # @param [Cinch::Message] m the message sent by a user.
  #
  # @since 4.2
  def stream_end(m)
    if @streams.include?(m.user)
      @streams.delete(m.user)
      m.reply("Your stream has been deleted. Yay, yay~!  ^_^")
    end
  end
end
