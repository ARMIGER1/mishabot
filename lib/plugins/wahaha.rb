# A class for handling random laughing.
#
# @author ARMIGER1
# @since 4.1
class Wahaha
  include Cinch::Plugin

  # @!attribute [rw] messages
  #   Keeps count of the number of channel messages submitted.
  # @!attribute [rw] laugh_value
  #   The number of channel messages after which MishaBot should laugh.
  attr_accessor :messages, :laugh_value

  listen_to :channel, method: :laugh

  def initialize(*args)
    super
    reset_messages
    reset_laugh_value
  end

  # Laughs after a random amount of channel messages.
  #
  # @author ARMIGER1
  def laugh(m)
    @messages += 1

    if @messages == @laugh_value
      m.reply "#{LAUGH}"
      reset_messages
      reset_laugh_value
    end
  end

  private

  def reset_messages
    @messages = 0
  end

  def reset_laugh_value
    @laugh_value = Random.rand(50..100)
  end
end
