# A class for the FEELS command.
#
# @author ARMIGER1
# @since 0.2
class Feels
  include Cinch::Plugin

  match(/^feels$/i, react_on: :private, method: :feels)

  def initialize(*args)
    super

    @rate_limited = []

    $mishabot.config.plugins.options[Feels] = {
      plugin: 'feels',
      commands: {
        feels: {
          help_message: 'Lets all users know you have feels to share.',
          syntax: 'FEELS',
          examples: '/msg MishaBot FEELS'
        }
      }
    }
  end

  # Sends a message to all users that the current user wants to share their
  # feels.
  #
  # @param [Cinch::Message] m the message sent by a user.
  def feels(m)
    message = <<-EOT.gsub(/^ {4}/) { '' }.chomp
    #{m.user} wants to share their feels!
    Everyone be respectful or incur the wrath of the Student Council! #{LAUGH}
    EOT

    @channel = Channel("#{CHANNEL}")

    unless @rate_limited.include?(m.user)
      @rate_limited << m.user

      @channel.send(message)

      rate_limit 30, 1 do
        @rate_limited.delete(m.user)
        m.reply "You can use the FEELS command again.  Yay-yay!  ^_^"
      end
    end

  end
end
