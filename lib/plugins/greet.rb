class Cinch::User
  def greetable?
    self != @bot
  end
end

# A class for handling user greetings
#
# @author ARMIGER1
# @since 0.1
class Greet
  include Cinch::Plugin

  listen_to :connect, method: :on_connect
  listen_to :join, method: :on_join
  listen_to :leaving, method: :on_leave

  HELP_COMMAND = '/msg MishaBot help'
  HELP_MSG = "Type '#{HELP_COMMAND}' to see what I can do. #{LAUGH}"

  def initialize(*args)
    super
    @greeted_users = {}
  end

  # Sends a greeting to a newly joined-user.
  #
  # @author ARMIGER1
  #
  # @param [Cinch::Message] m the message sent by the newly-joined user.
  def on_join(m)
    message = <<-EOT.gsub(/^ {4}/) { '' }.chomp
    Hi, #{m.user}, welcome to #katawashoujo!
    #{HELP_MSG}
    EOT

    unless greeted?(m.user)
      @greeted_users[m.user] = false
    end

    if greeted?(m.user) && @greeted_users[m.user] == false
      @greeted_users[m.user] = true

      # Rate limit set to 20 minutes.
      rate_limit (20 * 60), 1 do
        @greeted_users[m.user] = false
      end

      m.reply "#{message}" if m.user.greetable?
    end
  end

  # Misses a user who has parted the channel.
  #
  # @author ARMIGER1
  #
  # @param [Cinch::Message] m the parting message.
  # @param [Cinch::User] user the user who is leaving.
  def on_leave(m, user)
    username = user
    Timer(Random.rand(20..30), shots: 1) do
      Channel("#{CHANNEL}").send("Aww... I miss #{username}... :(") unless Channel("#{CHANNEL}").users.include?(username)
    end
  end

  # Sends a message upon connecting to a channel.
  #
  # @author ARMIGER1
  #
  # @param [Cinch::Message] _m unused.
  def on_connect(_m)
    message = <<-EOT.gsub(/^ {4}/) { '' }.chomp
    Hi there, I'm #{@bot.config.nick}!
    #{HELP_MSG}
    EOT

    Channel("#{CHANNEL}").send(message)
  end

  private

  # Checks if the specified user has been granted.
  #
  # @author ARMIGER1
  #
  # @since (see #increment_login)
  def greeted?(user)
    @greeted_users.keys.include?(user)
  end
end
