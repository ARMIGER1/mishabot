# Class for the main help plugin.
#
# @author ARMIGER1
# @since 4.0
class Help
  include Cinch::Plugin

  match(/help$/i, react_on: :private, method: :help)
  match(/help (?<command>\w+)/i, react_on: :private, method: :command)

  def initialize(*args)
    super
    @bot_plugin_options = $mishabot.config.plugins.options
  end

  # Sends a user a private message containing the main help message.
  #
  # @author ARMIGER1
  #
  # @param [Cinch::Message] m message sent by a user.
  def help(m)

    help_hash = {}

    @bot_plugin_options.each_key do |key|
      @bot_plugin_options[key][:commands].each_key do |cmd_key|
        help_hash[cmd_key] = @bot_plugin_options[key][:commands][cmd_key][:help_message]
      end
    end

    m.user.send command_help_message(help_hash)
  end

  # Sends a user a private message containing a specific command's help message.
  #
  # @author ARMIGER1
  #
  # @param (see #help)
  # @param [String] command command whose help message to show.
  def command(m, command)

    @bot_plugin_options.each_key do |key|
      if @bot_plugin_options[key][:commands].keys.include?(command.downcase.to_sym)

        command_hash = @bot_plugin_options[key][:commands][command.downcase.to_sym]

        m.user.send help_message(
          command.upcase,
          command_hash[:help_message],
          command_hash[:syntax],
          command_hash[:examples],
          command_hash[:notes]
        )
      end
    end

  end
end
