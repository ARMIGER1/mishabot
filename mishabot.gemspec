Gem::Specification.new do |s|
  s.name = 'mishabot'
  s.version = '4.0'
  s.date = '2014-01-16'
  s.summary = 'MishaBot: A Bot of Infinite Potential'
  s.description = "A cinch-based bot that provides various services to /r/katawashoujo's Freenode IRC channel."
  s.authors = ['ARMIGER1']
  s.email = '1hockeyplayer+bitbucket@gmail.com'
  s.files = ['lib/mishabot.rb']
  s.homepage = 'BITBUCKET_PROJECT_PAGE'
end
